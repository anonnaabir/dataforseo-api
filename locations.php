<?php
// You can download this file from here https://cdn.dataforseo.com/v3/examples/php/php_RestClient.zip
// require('RestClient.php');
$api_url = 'https://api.dataforseo.com/';
  $client = new RestClient($api_url, null, 'jarnott@contentfirst.marketing', '573cec560e192819');

try {
  // using this method you can get a list of locations
  // GET /v3/serp/google/locations
  // in addition to 'google' you can also set other search engine
  // the full list of possible parameters is available in documentation
  $locations = $client->get('/v3/dataforseo_labs/locations_and_languages');
  // print_r($locations);
  // do something with result
} catch (RestClientException $e) {
  echo "\n";
  print "HTTP code: {$e->getHttpCode()}\n";
  print "Error code: {$e->getCode()}\n";
  print "Message: {$e->getMessage()}\n";
  print  $e->getTraceAsString();
  echo "\n";
}
$client = null;
?>
