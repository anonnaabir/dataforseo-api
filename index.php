  <!DOCTYPE html>
  <html lang="en">
  <head>
    <title>Data For SEO</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  </head>
  <body>

        <?php
        error_reporting(0);
        // You can download this file from here https://cdn.dataforseo.com/v3/examples/php/php_RestClient.zip
        require('RestClient.php');
        require('locations.php');
        $api_url = 'https://api.dataforseo.com/';

        try {
          // Instead of 'login' and 'password' use your credentials from https://app.dataforseo.com/api-dashboard
          $client = new RestClient($api_url, null, 'jarnott@contentfirst.marketing', '573cec560e192819');
        } catch (RestClientException $e) {
          echo "\n";
          print "HTTP code: {$e->getHttpCode()}\n";
          print "Error code: {$e->getCode()}\n";
          print "Message: {$e->getMessage()}\n";
          print  $e->getTraceAsString();
          echo "\n";
          exit();
        }

        // echo $location_list = $locations['tasks'][0]['result'][1]['location_code'];

        ?>

        <!-- <input type="radio" id="api_keyword" name="keyword" value="keyword">
        <label for="male">Keyword Search</label><br>

        <input type="radio" id="api_domain" name="domain" value="domain">
        <label for="male">Domain Search</label><br>
   -->

        <div class="container">
          <h2>Data For SEO</h2>
          <form action="index.php" method="post">
            <div class="form-group h-100">
              <label for="keyword">Keyword:</label>
              <input type="text" class="form-control" id="keyword_search" placeholder="Enter keyword" name="keyword_search">
            </div>

            <select class="form-control" name="location_code" id="location_code">
          <?php

            $location_list = $locations['tasks'][0]['result'];

            for ($i=0; $i <= 80 ; $i++) {
                $select_location = '<option value="'.$location_list[$i]['location_code'].'">'.$location_list[$i]['location_name'].'</option>';
                echo $select_location;
            }

            ?>
            </select>
            <br>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>


        <?php

        $post_array = array();

        // You can set only one task at a time
        $getkeyword = $_POST["keyword_search"];
        $getlocation = $_POST["location_code"];

        $post_array[] = array(
          "language_code" => "en",
          "location_code" => $getlocation,
          "keyword" => mb_convert_encoding($getkeyword, "UTF-8")
        );

        if(isset($getkeyword)) {

        try {
          // POST /v3/serp/google/organic/live/regular
          // in addition to 'google' and 'organic' you can also set other search engine and type parameters
          // the full list of possible parameters is available in documentation
          $result = $client->post('/v3/serp/google/organic/live/regular', $post_array);
          // echo $result['tasks'][0]['result'][0]['items'][3]['url'];
          $allitems = $result['tasks'][0]['result'][0]['items'];

          // print_r($locations);

          ?>
          <br><br><br>
          <div class="container">
          <table class="table table-bordered">
            <tbody>
              <tr>
              <td>RANK ABSOLUTE</td>
              <td>DOMAIN</td>
              <td>URL</td>
              </tr>

              <?php

              for ($i=0; $i <= 50 ; $i++) {
                echo '
                <tr>
                <td>'.$allitems[$i]['rank_absolute'].'</td>
                <td>'.$allitems[$i]['domain'].'</td>
                <td><a target="_blank" href="'.$allitems[$i]['url'].'">View URL</a></td>
                </tr>';
              }

              ;?>

            </tbody>
          </table>
        </div>

          <?php

        } catch (RestClientException $e) {
          echo "\n";
          print "HTTP code: {$e->getHttpCode()}\n";
          print "Error code: {$e->getCode()}\n";
          print "Message: {$e->getMessage()}\n";
          print  $e->getTraceAsString();
          echo "\n";
        }
        $client = null;


        }
        ?>


      </body>
      </html>
