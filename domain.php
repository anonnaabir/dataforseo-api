    <!DOCTYPE html>
    <html lang="en">
    <head>
      <title>Data For SEO</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>
    <body>

    <?php
    // You can download this file from here https://cdn.dataforseo.com/v3/examples/php/php_RestClient.
    error_reporting(0);
    require('RestClient.php');
    require('locations.php');
    $api_url = 'https://api.dataforseo.com/';
    // Instead of 'login' and 'password' use your credentials from https://app.dataforseo.com/api-dashboard
    $client = new RestClient($api_url, null, 'jarnott@contentfirst.marketing', '573cec560e192819');

    ?>

    <div class="container">
      <h2>Data For SEO Domain</h2>
      <form action="domain.php" method="post">
        <div class="form-group h-100">
          <label for="keyword">Domain:</label>
          <input type="text" class="form-control" id="domain_search" placeholder="Enter Domain" name="domain_search">
        </div>

        <select class="form-control" name="location_code" id="location_code">
      <?php

        $location_list = $locations['tasks'][0]['result'];

        for ($i=0; $i <= 80 ; $i++) {
            $select_location = '<option value="'.$location_list[$i]['location_code'].'">'.$location_list[$i]['location_name'].'</option>';
            echo $select_location;
        }

        ?>
        </select>
        <br>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>

    <?php

    $getdomain = $_POST["domain_search"];
    $getlocation = $_POST["location_code"];

    $post_array = array();
    // simple way to set a task
    $post_array[] = array(
       "target" => $getdomain,
       "language_name" => "English",
       "location_code" => $getlocation,
       "filters" => [
          ["keyword_data.keyword_info.search_volume", "<>", 0],
          "and",
          [
             ["ranked_serp_element.serp_item.type", "<>", "paid"],
             "or",
             ["ranked_serp_element.serp_item.is_malicious", "=", false]
          ]
       ]
    );

    if(isset($getdomain)) {

    try {
       // POST /v3/dataforseo_labs/ranked_keywords/live
       $result = $client->post('/v3/dataforseo_labs/ranked_keywords/live', $post_array);
       $metrics = $result['tasks'][0]['result'][0]['items'];
       // $rank = $result['tasks'][0]['result'][0]['items'][0]['ranked_serp_element']['serp_item']['rank_absolute'];
       // $url = $result['tasks'][0]['result'][0]['items'][0]['ranked_serp_element']['serp_item']['url'];
       // $url = $result['tasks'][0]['result'][0]['items'][0]['ranked_serp_element']['serp_item']['url'];
       // $keyword = $result['tasks'][0]['result'][0]['items'][0]['keyword_data']['keyword'];
       // echo "<pre>";
       // echo "<h1>The output: ".$url."</h1>";
       // echo "</pre>";

       ?>

       <br><br><br>
       <div class="container">
       <table class="table table-bordered">
         <tbody>
           <tr>
           <td>KEYWORD</td>
           <td>RANK ABSOLUTE</td>
           <td>URL</td>
           </tr>

           <?php

           for ($i=0; $i <= 50 ; $i++) {
             echo '
             <tr>
             <td>'.$metrics[$i]['keyword_data']['keyword'].'</td>
             <td>'.$metrics[$i]['ranked_serp_element']['serp_item']['rank_absolute'].'</td>
             <td><a target="_blank" href="'.$metrics[$i]['ranked_serp_element']['serp_item']['url'].'">View URL</a></td>
             </tr>';
           }

           ;?>

         </tbody>
       </table>
     </div>

       <?php
       // print_r($result);
       // do something with post result
    } catch (RestClientException $e) {
       echo "\n";
       print "HTTP code: {$e->getHttpCode()}\n";
       print "Error code: {$e->getCode()}\n";
       print "Message: {$e->getMessage()}\n";
       print  $e->getTraceAsString();
       echo "\n";
    }
    $client = null;

  }

    // require('RestClient.php');
    // require('locations.php');


    ?>


    </body>
    </html>
